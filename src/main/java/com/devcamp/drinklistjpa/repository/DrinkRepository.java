package com.devcamp.drinklistjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.drinklistjpa.model.CDrink;

public interface DrinkRepository extends JpaRepository<CDrink, Long>{
    CDrink findById(long id);
}
