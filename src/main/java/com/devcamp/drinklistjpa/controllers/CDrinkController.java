package com.devcamp.drinklistjpa.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.drinklistjpa.model.CDrink;
import com.devcamp.drinklistjpa.repository.DrinkRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    DrinkRepository pDrinkRepository;
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getDrinks(){
        try {
            List<CDrink> drinkList = new ArrayList<CDrink>();
            pDrinkRepository.findAll().forEach(drinkList::add);
            if (drinkList.size()==0){
                return new ResponseEntity<>(drinkList,HttpStatus.NOT_FOUND);
            }
            else return new ResponseEntity<>(drinkList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id){
        try {
            CDrink drinkData = pDrinkRepository.findById(id);
            return new ResponseEntity<>(drinkData, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pDrinks){
        try {
            pDrinks.setNgayTao(new Date());
            pDrinks.setNgayCapNhat(null);
            CDrink _drinks = pDrinkRepository.save(pDrinks);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrinks){
        try {
            CDrink drink = pDrinkRepository.findById(id);
            if (drink != null){
                drink.setMaNuocUong(pDrinks.getMaNuocUong());
                drink.setTenNuocUong(pDrinks.getTenNuocUong());
                drink.setPrice(pDrinks.getPrice());
                drink.setGhiChu(pDrinks.getGhiChu());
                drink.setNgayCapNhat(new Date());
                return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id){
        try{
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
