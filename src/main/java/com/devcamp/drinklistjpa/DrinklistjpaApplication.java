package com.devcamp.drinklistjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DrinklistjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrinklistjpaApplication.class, args);
	}

}
